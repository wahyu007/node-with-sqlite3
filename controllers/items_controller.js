const fs = require('fs');
const qs = require('querystring');
const sqlite3 = require('sqlite3');

const items = [];
const db = new sqlite3.Database("./db/development.db");

db.all("SELECT * FROM items", (error, rows) => {
    if(error) throw error;
    for (const row of rows){
        items.push(row.value);
    }
});

const itemsController = (req, res) => {
    if(req.url === '/items') {

        if (req.method === 'POST') {
            let body = '';
            req.on("data", chuck => {
                body = `${body}${chuck.toString()}`;
            })

            // req.on("end", () => {items.push(qs.parse(body).value)});
            req.on("end", ()=>{
                const parseBody = qs.parse(body);
                items.push(parseBody.value);
                const insert = db.prepare("INSERT INTO items VALUES(?)");
                insert.run(parseBody.value);
                insert.finalize();
            });

            res.writeHead(301, {Location: "/items" });
            return res.end();
        }
        else{
            fs.readFile('./views/items/index.html', (error, html) => {
                if(error) throw error;
                res.write(html);
                res.write("<ul>");
                for (const item of items) {
                    res.write(`<li>${item}</li>`);
                }
                res.write('<ul>');
                return res.end();
            })
        }
    }

    if(req.url === '/items/new') {
        fs.readFile('./views/items/new.html', (error, html) => {
            if(error) throw error;
            res.write(html);
            return res.end();
        })
    }
}

module.exports = itemsController;