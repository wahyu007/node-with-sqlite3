const fs = require('fs')
const itemsController = require('./items_controller');

const appController = (req, res) => {
    if(req.url === '/' ){
        fs.readFile("./views/index.html", (error, html) => {
            if(error) throw error;
            res.write(html);
            return res.end();
        })
    }
    itemsController(req, res);
}

module.exports = appController;